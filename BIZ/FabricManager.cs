﻿using BIZ.Managers;
using COMMON.Validadores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BIZ
{
    public static  class FabricManager
    {
        public static CarreraManager CarreraManager() => new CarreraManager(new CarreraValidator());
        public static RolManager RolManager()=>new RolManager(new RolValidator());

        public static UsuariosManager UsuariosManager() => new UsuariosManager(new UsuariosValidator());
    }
}

﻿using COMMON.Entidades;
using COMMON.Modelos;
using COMMON.Validadores;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BIZ.Managers
{
    public class UsuariosManager : GenericManager<Usuarios>
    {
        public UsuariosManager(GenericValidator<Usuarios> validator) : base(validator)
        {
        }

        public List<ModelUsuarioListado> ObtenerTodosListados() => ObtenerTodosListadosAsync().Result;

        private async Task<List<ModelUsuarioListado>> ObtenerTodosListadosAsync()
        {
            HttpResponseMessage response = await client.GetAsync("Usuarios/ObtenerUsuariosCompletos").ConfigureAwait(false);
            var content = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
            if (response.IsSuccessStatusCode)
            {
                Error = "";
                return JsonConvert.DeserializeObject<List<ModelUsuarioListado>>(content);
            }
            else
            {
                Error = content;
                return null;
            }
        }

        public Usuarios Login(Login login) => LoginAsync(login).Result;
        

        private async Task<Usuarios> LoginAsync(Login login)
        {
            var body = new StringContent(JsonConvert.SerializeObject(login), Encoding.UTF8, "application/json");
            HttpResponseMessage response = await client.PostAsync("Usuarios/Login", body).ConfigureAwait(false);
            var content = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
            if (response.IsSuccessStatusCode)
            {
                Error = "";
                return JsonConvert.DeserializeObject<Usuarios>(content);
            }
            else
            {
                Error = content;
                return null;
            }
        }
    }
}

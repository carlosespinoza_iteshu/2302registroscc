﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COMMON.Entidades
{
    public class Carrera:Base
    {
        public string Nombre { get; set; }
        public string NombreCoordinador { get; set; }
        public override string ToString()
        {
            return $"{Nombre} - {NombreCoordinador}";
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COMMON.Entidades
{
    public class DetalleDeRegistro:Base
    {
        public string IdRegistro { get; set; }
        public DateTime Fecha { get; set; }
        public DateTime HoraEntrada { get; set; }
        public DateTime HoraSalida { get; set; }
        public int NumClase { get; set; }
        public string IdSoftwareEquipo { get; set; }
        public string Descripcion { get; set; }
        public int NumAlumnos { get; set; }
        public string Observaciones { get; set; }
        public int HorasPracticas { get; set; }
        public int HorasTeoricas { get; set; }

    }
}

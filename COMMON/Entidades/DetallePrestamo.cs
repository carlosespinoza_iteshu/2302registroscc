﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COMMON.Entidades
{
    public class DetallePrestamo:Base
    {
        public string Descripcion { get; set; }
        public string NumInventario { get; set; }
        public int Cantidad { get; set; }
        public string IdRegistroPrestamo { get; set; }
    }
}

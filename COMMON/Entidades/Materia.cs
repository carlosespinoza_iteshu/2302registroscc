﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COMMON.Entidades
{
    public class Materia:Base
    {
        public string Nombre { get; set; }
        public int HorasPracticas { get; set; }
        public int HorasTeoricas { get; set; }
    }
}

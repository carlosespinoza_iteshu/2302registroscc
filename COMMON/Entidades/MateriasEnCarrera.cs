﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COMMON.Entidades
{
    public class MateriasEnCarrera:Base
    {
        public string IdCarrera { get; set; }
        public string IdMateria { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COMMON.Entidades
{
    public class RegistroDeClase:Base
    {
        public string ClaveLaboratorio { get; set; }
        public string Periodo { get; set; }
        public string IdMateria { get; set; }
        public string IdCarrera { get; set; }
        public int Semestre { get; set; }
        public string ClaveDocente { get; set; }
    }
}

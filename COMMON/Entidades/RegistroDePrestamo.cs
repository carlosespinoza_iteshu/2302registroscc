﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COMMON.Entidades
{
    public class RegistroDePrestamo:Base
    {
        public string IdCarrera { get; set; }
        public string IdLaboratorio { get; set; }
        public string AreaLaboratorio { get; set; }
        public DateTime FechaSolicitud { get; set; }
        public DateTime FechaDevolucion { get; set; }
        public DateTime? FechaDevolucionReal { get; set; }
        public string ObservacionesArea { get; set; }
        public string ObservacionesUnidad { get; set; }
        public string ObservacionesResponsable { get; set; }
        public string PrestamoRecepcion { get; set; }
        public string EntregaRecepcion { get; set; }
    }
}

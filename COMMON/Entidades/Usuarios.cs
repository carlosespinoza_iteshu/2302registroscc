﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COMMON.Entidades
{
    public class Usuarios:Base
    {
        public string Matricula { get; set; }
        public string Nombre { get; set; }
        public string Apellidos { get; set; }
        public string IdCarrera { get; set; }
        public string Password { get; set; }
        public bool Habilitado { get; set; }
        public string IdRol { get; set; }

        public override string ToString()
        {
            return $"{Matricula}-{Apellidos} {Nombre}";
        }
    }
}

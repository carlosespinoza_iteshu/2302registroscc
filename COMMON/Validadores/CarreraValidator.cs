﻿using COMMON.Entidades;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COMMON.Validadores
{
    public class CarreraValidator:GenericValidator<Carrera>
    {
        public CarreraValidator()
        {
            RuleFor(c => c.Nombre).NotEmpty().MaximumLength(50);
            RuleFor(c => c.NombreCoordinador).NotEmpty().MaximumLength(100);

        }
    }
}

﻿using COMMON.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentValidation;

namespace COMMON.Validadores
{
    public class DetallePrestamoValidator:GenericValidator<DetallePrestamo>
    {
        public DetallePrestamoValidator()
        {
            RuleFor(d => d.Descripcion).NotEmpty().MaximumLength(500);
            RuleFor(d=>d.NumInventario).NotEmpty().MaximumLength(100);
            RuleFor(d => d.Cantidad).GreaterThan(0);
            RuleFor(d => d.IdRegistroPrestamo).NotEmpty();

        }
    }
}

﻿using COMMON.Entidades;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COMMON.Validadores
{
    public class DetalleRegistroValidator:GenericValidator<DetalleDeRegistro>
    {
        public DetalleRegistroValidator()
        {
            RuleFor(d => d.IdRegistro).NotEmpty();
            RuleFor(d=>d.Fecha).NotEmpty();
            RuleFor(d=>d.HoraEntrada).NotEmpty();
            RuleFor(d=>d.HoraSalida).NotEmpty();
            RuleFor(d => d.NumClase).GreaterThan(0);
            RuleFor(d=>d.Descripcion).MaximumLength(500).When(d=>d.Descripcion!=null);
            RuleFor(d => d.NumClase).GreaterThan(0);
            RuleFor(d=>d.Observaciones).MaximumLength(500).When(d=>d.Descripcion!=null);
            RuleFor(d => d.HorasTeoricas).GreaterThanOrEqualTo(0);
            RuleFor(d => d.HorasPracticas).GreaterThanOrEqualTo(0);
    }
    }
}

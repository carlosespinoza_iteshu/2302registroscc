﻿using FluentValidation;
using COMMON.Entidades;

namespace COMMON.Validadores
{
    public abstract class GenericValidator<T> :AbstractValidator<T> where T : Base
    {
        public GenericValidator()
        {
            RuleFor(x => x.Id).NotEmpty();
        }
    }
}

﻿using COMMON.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentValidation;

namespace COMMON.Validadores
{
    public class LaboratorioValidator:GenericValidator<Laboratorio>
    {
        public LaboratorioValidator()
        {
            RuleFor(l => l.NumComputadora).GreaterThan(0);
            RuleFor(l => l.IdCarrera).NotEmpty();
            RuleFor(l => l.Nombre).NotEmpty().MaximumLength(50);

        }
    }
}

﻿using COMMON.Entidades;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COMMON.Validadores
{
    public class MateriaEnCarreraValidator:GenericValidator<MateriasEnCarrera>
    {
        public MateriaEnCarreraValidator()
        {
            RuleFor(m => m.IdCarrera).NotEmpty();
            RuleFor(m => m.IdMateria).NotEmpty();
        }
    }
}

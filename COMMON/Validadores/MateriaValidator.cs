﻿using COMMON.Entidades;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COMMON.Validadores
{
    public class MateriaValidator:GenericValidator<Materia>
    {
        public MateriaValidator()
        {
            RuleFor(m => m.Nombre).NotEmpty().MaximumLength(50);
            RuleFor(m => m.HorasPracticas).GreaterThan(0);
            RuleFor(m => m.HorasTeoricas).GreaterThan(0);
        }
    }
}

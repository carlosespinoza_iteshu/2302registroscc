﻿using COMMON.Entidades;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COMMON.Validadores
{
    public class RegistroDeClaseValidator:GenericValidator<RegistroDeClase>
    {
        public RegistroDeClaseValidator()
        {
            RuleFor(r => r.ClaveLaboratorio).NotEmpty();
            RuleFor(r => r.Periodo).NotEmpty().MaximumLength(50);
            RuleFor(r => r.IdMateria).NotEmpty().MaximumLength(10);
            RuleFor(r => r.IdCarrera).NotEmpty().MaximumLength(10);
            RuleFor(r => r.Semestre).GreaterThan(0);
            RuleFor(r => r.ClaveDocente).NotEmpty();

        }
    }
}

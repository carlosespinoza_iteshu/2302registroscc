﻿using COMMON.Entidades;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COMMON.Validadores
{
    public class RegistroDePrestamoValidator:GenericValidator<RegistroDePrestamo>
    {
        public RegistroDePrestamoValidator()
        {
            RuleFor(r => r.IdCarrera).NotEmpty();
            RuleFor(r => r.IdLaboratorio).NotEmpty();
            RuleFor(r => r.AreaLaboratorio).NotEmpty().MaximumLength(100);
            RuleFor(r => r.FechaSolicitud).NotEmpty();
            RuleFor(r => r.FechaDevolucion).NotEmpty();
            RuleFor(r => r.PrestamoRecepcion).NotEmpty().MaximumLength(10);
            RuleFor(r => r.EntregaRecepcion).NotEmpty().MaximumLength(10);


        }
    }
}

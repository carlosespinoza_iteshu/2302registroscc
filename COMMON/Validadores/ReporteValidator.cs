﻿using COMMON.Entidades;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COMMON.Validadores
{
    public class ReporteValidator:GenericValidator<Reporte>
    {
        public ReporteValidator()
        {
            RuleFor(r => r.Actividades).NotEmpty();
            RuleFor(r=>r.FechaInicio).NotEmpty();
            RuleFor(r=>r.FechaTermino).NotEmpty();
            RuleFor(r=>r.IdCarrera).NotEmpty();
        }
    }
}

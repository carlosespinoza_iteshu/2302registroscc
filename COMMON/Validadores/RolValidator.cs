﻿using COMMON.Entidades;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COMMON.Validadores
{
    public class RolValidator:GenericValidator<Rol>
    {
        public RolValidator()
        {
            RuleFor(r => r.Nombre).NotEmpty().MaximumLength(50);
        }
    }
}

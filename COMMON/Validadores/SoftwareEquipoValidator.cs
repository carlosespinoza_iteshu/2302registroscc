﻿using COMMON.Entidades;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COMMON.Validadores
{
    public class SoftwareEquipoValidator:GenericValidator<SoftwareEquipo>
    {
        public SoftwareEquipoValidator() 
        {
            RuleFor(s => s.Nombre).NotEmpty().MaximumLength(100);
        }
    }
}

﻿using COMMON.Entidades;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COMMON.Validadores
{
    public class UsuariosValidator:GenericValidator<Usuarios>
    {
        public UsuariosValidator()
        {
            RuleFor(u => u.Matricula).NotEmpty().MaximumLength(10);
            RuleFor(u => u.Nombre).NotEmpty().MaximumLength(50);
            RuleFor(u => u.Apellidos).NotEmpty().MaximumLength(50);
            RuleFor(u => u.IdCarrera).NotEmpty();
            RuleFor(u => u.Password).NotEmpty();
            RuleFor(u => u.IdRol).NotEmpty();

        }
                
    }
}

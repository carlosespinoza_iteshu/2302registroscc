﻿using BIZ;
using BIZ.Managers;
using COMMON.Entidades;
using EjemploMaui.Pages;

namespace EjemploMaui
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }
        private void btnUsuarios_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new CatalogoUsuarios());
        }
    }
}
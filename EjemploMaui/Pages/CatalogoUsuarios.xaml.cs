using BIZ;
using BIZ.Managers;
using COMMON.Entidades;
using COMMON.Modelos;

namespace EjemploMaui.Pages;

public partial class CatalogoUsuarios : ContentPage
{
    UsuariosManager usuariosManager;
    List<Usuarios> usuarios;
    List<Rol> roles;
    List<Carrera> carreras;
    public CatalogoUsuarios()
    {
        InitializeComponent();
        usuariosManager = FabricManager.UsuariosManager();
        roles = FabricManager.RolManager().ObtenerTodos;
        carreras = FabricManager.CarreraManager().ObtenerTodos;
    }

    protected override void OnAppearing()
    {
        base.OnAppearing();
        lstUsuarios.ItemsSource = null;
        List<ModelUsuarioListado> listados = usuariosManager.ObtenerTodosListados();
        if (listados != null)
        {
            lstUsuarios.ItemsSource = listados;
        }
        else
        {
            DisplayAlert("Error", "Verifica tu conexi�n a Internet", "Ok");
        }
    }



    private void btnNuevoUsuario_Clicked(object sender, EventArgs e)
    {
        Navigation.PushAsync(new EditarUsuario(new COMMON.Entidades.Usuarios()));
    }

    private void lstUsuarios_ItemSelected(object sender, SelectedItemChangedEventArgs e)
    {
        Navigation.PushAsync(new EditarUsuario(e.SelectedItem as Usuarios));
    }
}
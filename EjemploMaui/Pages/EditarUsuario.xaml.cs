using BIZ;
using BIZ.Managers;
using COMMON.Entidades;

namespace EjemploMaui.Pages;

public partial class EditarUsuario : ContentPage
{
	Usuarios usuario;
	UsuariosManager usuariosManager;
	CarreraManager carreraManager;
	RolManager rolManager;
	bool esNuevo;
	public EditarUsuario(Usuarios usuario)
	{
		InitializeComponent();
		this.BindingContext= usuario;
		this.usuario= usuario;
		esNuevo = string.IsNullOrEmpty(usuario.Id);
		carreraManager = FabricManager.CarreraManager();
		usuariosManager = FabricManager.UsuariosManager();
		rolManager = FabricManager.RolManager();
		List<Carrera> carreras= carreraManager.ObtenerTodos;
		pkrCarrera.ItemsSource = carreras;
		List<Rol> roles= rolManager.ObtenerTodos;
		pkrRol.ItemsSource = roles;
		btnEliminar.IsVisible = !esNuevo;
		if (!esNuevo)
		{
			pkrCarrera.SelectedItem = carreras.Where(c => c.Id == usuario.IdCarrera).SingleOrDefault();
			pkrRol.SelectedItem=roles.Where(r=>r.Id==usuario.IdRol).SingleOrDefault();
		}

	}

    private void btnGuardar_Clicked(object sender, EventArgs e)
    {
		Usuarios u = this.BindingContext as Usuarios;
		Carrera c=pkrCarrera.SelectedItem as Carrera;
		Rol r=pkrRol.SelectedItem as Rol;
		u.IdCarrera = c.Id;
		u.IdRol=r.Id;
		u.Habilitado = true;
		if (u.Password == entPassword2.Text)
		{
			//Guardar
			Usuarios result = esNuevo ? usuariosManager.Insertar(u) : usuariosManager.Modificar(u, u.Id);
			if (result != null)
			{
				DisplayAlert("�xito", "Datos guardados correctamente", "Ok");
				Navigation.PopAsync();
			}
			else
			{
                DisplayAlert("Error", usuariosManager.Error, "Ok");
            }
		}
		else
		{
			DisplayAlert("Error", "Las contrase�as no son iguales", "Ok");
		}
    }

	private async void btnEliminar_Clicked(object sender, EventArgs e)
	{
		try
		{
            var r = await DisplayAlert("Confirma", "�Estas seguro de eliminar este registro?", "Si", "No");
            if (r)
            {
                if (usuariosManager.Eliminar(usuario.Id))
                {
                    await DisplayAlert("�xito", "Registro eliminado correctamente", "Ok");
                    await Navigation.PopAsync();
                }
                else
                {
                    await DisplayAlert("Error", usuariosManager.Error, "Ok");
                }

            }
        }
		catch (Exception ex)
		{
			await DisplayAlert("Error", ex.Message, "Ok");
		}
		
	}
}
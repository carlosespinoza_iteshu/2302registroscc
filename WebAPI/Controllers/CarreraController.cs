﻿using COMMON.Entidades;
using COMMON.Validadores;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CarreraController : GenericController<Carrera>
    {
        public CarreraController() : base(new CarreraValidator())
        {
        }
    }
}

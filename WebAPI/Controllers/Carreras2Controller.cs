﻿using COMMON.Entidades;
using COMMON.Validadores;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class Carreras2Controller : GenericSQLServerController<Carrera>
    {
        public Carreras2Controller() : base(new CarreraValidator())
        {
        }
    }
}

﻿using LiteDB;
using Microsoft.AspNetCore.Mvc;
using COMMON.Entidades;
using COMMON.Validadores;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public abstract class GenericController<T> : ControllerBase, IGenericController<T> where T : Base
    {
        protected string DBName = "RegistrosCC.db";
        private GenericValidator<T> validator;
        public GenericController(GenericValidator<T> validador)
        {
            validator = validador;
        }

        protected List<M> ObtenerDatos<M>() where M:class
        {
            List<M> datos = new List<M>();
            using (var db = new LiteDatabase(DBName))
            {
                var col = db.GetCollection<M>(typeof(M).Name);
                datos = col.FindAll().ToList();
            }
            return datos;
        }

        // GET: api/<GenericController>
        [HttpGet]
        public ActionResult<List<T>> Get()
        {
            try
            {
                List<T> datos = new List<T>();
                using (var db = new LiteDatabase(DBName))
                {
                    var col = db.GetCollection<T>(typeof(T).Name);
                    datos = col.FindAll().ToList();
                }
                return Ok(datos);
            }
            catch (Exception)
            {
                return BadRequest(null);
            }
        }


        // GET api/<ContactosController>/5
        [HttpGet("{id}")]
        public ActionResult<T> Get(string id)
        {
            try
            {
                T dato;
                using (var db = new LiteDatabase(DBName))
                {
                    var col = db.GetCollection<T>(typeof(T).Name);
                    dato = col.FindById(id);
                }
                return Ok(dato);
            }
            catch (Exception)
            {
                return BadRequest(null);
            }
        }




        // POST api/<ContactosController>
        [HttpPost]
        public ActionResult<T> Post([FromBody] T value)
        {
            try
            {
                value.Id = Guid.NewGuid().ToString();
                var resultadoValidacion = validator.Validate(value);
                if (resultadoValidacion.IsValid)
                {
                    using (var db = new LiteDatabase(DBName))
                    {
                        var col = db.GetCollection<T>(typeof(T).Name);
                        col.Insert(value);
                    }
                    return Get(value.Id);
                }
                else
                {
                    string error = "";
                    foreach (var item in resultadoValidacion.Errors)
                    {
                        error += item.ErrorMessage + ". ";
                    }
                    return BadRequest(error);
                }
            }
            catch (Exception)
            {
                return BadRequest(false);
            }

        }

        // PUT api/<ContactosController>/5
        [HttpPut("{id}")]
        public ActionResult<T> Put(string id, [FromBody] T value)
        {
            //Actualizar un registro
            try
            {
                var resultadoValidacion = validator.Validate(value);
                if (resultadoValidacion.IsValid)
                {
                    using (var db = new LiteDatabase(DBName))
                    {
                        var col = db.GetCollection<T>(typeof(T).Name);
                        col.Update(id, value);
                    }
                    return Get(id);
                }
                else
                {
                    string error = "";
                    foreach (var item in resultadoValidacion.Errors)
                    {
                        error += item.ErrorMessage + ". ";
                    }
                    return BadRequest(error);
                }
            }
            catch (Exception)
            {
                return BadRequest(false);
            }
        }

        // DELETE api/<ContactosController>/5
        [HttpDelete("{id}")]
        public ActionResult<bool> Delete(string id)
        {
            //eliminar un registro
            try
            {
                using (var db = new LiteDatabase(DBName))
                {
                    var col = db.GetCollection<T>(typeof(T).Name);
                    col.Delete(id);
                }
                return Ok(true);
            }
            catch (Exception)
            {
                return BadRequest(false);
            }
        }
    }
}

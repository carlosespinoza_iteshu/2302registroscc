﻿using COMMON.Entidades;
using COMMON.Validadores;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Driver;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public abstract class GenericMongoController<T> : ControllerBase, IGenericController<T> where T : Base
    {

        MongoClient mongoClient;
        IMongoDatabase db;
        private GenericValidator<T> validator;

        protected IMongoCollection<T> Coleccion()
        {
            return db.GetCollection<T>(typeof(T).Name);
        }

        public GenericMongoController(GenericValidator<T> validador)
        {
            this.validator = validador;
            mongoClient = new MongoClient(@"cadena de conexión a mongodb");
            db = mongoClient.GetDatabase("NombreDeBD");

        }
        // GET: api/<GenericController>
        [HttpGet]
        public ActionResult<List<T>> Get()
        {
            try
            {
                List<T> datos = Coleccion().AsQueryable().ToList();
                return Ok(datos);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }


        // GET api/<ContactosController>/5
        [HttpGet("{id}")]
        public ActionResult<T> Get(string id)
        {
            try
            {
                T dato;
                dato = Coleccion().Find(d => d.Id == id).SingleOrDefault();
                return Ok(dato);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }




        // POST api/<ContactosController>
        [HttpPost]
        public ActionResult<T> Post([FromBody] T value)
        {
            try
            {
                value.Id = Guid.NewGuid().ToString();
                var resultadoValidacion = validator.Validate(value);
                if (resultadoValidacion.IsValid)
                {
                    Coleccion().InsertOne(value);
                    return Get(value.Id);
                }
                else
                {
                    string error = "";
                    foreach (var item in resultadoValidacion.Errors)
                    {
                        error += item.ErrorMessage + ". ";
                    }
                    return BadRequest(error);
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

        }

        // PUT api/<ContactosController>/5
        [HttpPut("{id}")]
        public ActionResult<T> Put(string id, [FromBody] T value)
        {
            //Actualizar un registro
            try
            {
                var resultadoValidacion = validator.Validate(value);
                if (resultadoValidacion.IsValid)
                {
                    Coleccion().ReplaceOne(id, value);
                    return Get(id);
                }
                else
                {
                    string error = "";
                    foreach (var item in resultadoValidacion.Errors)
                    {
                        error += item.ErrorMessage + ". ";
                    }
                    return BadRequest(error);
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        // DELETE api/<ContactosController>/5
        [HttpDelete("{id}")]
        public ActionResult<bool> Delete(string id)
        {
            //eliminar un registro
            try
            {
                Coleccion().DeleteOne(id);
                return Ok(true);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}

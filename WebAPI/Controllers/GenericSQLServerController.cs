﻿using COMMON.Entidades;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using System.Data;
using System.Linq;
using System.Reflection;
using Newtonsoft.Json;
using System.Text;
using COMMON.Validadores;
using FluentValidation.Results;


namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public abstract class GenericSQLServerController<T> : ControllerBase, IGenericController<T> where T : Base
    {
        private SqlConnection conexion;
        //private string cadConexion=@"Colocar tu cadena de conexión";
        private string cadConexion = @"Data Source=200.79.179.167;Initial Catalog=Pruebas;User ID=sa;Password=Iteshu!2022;Persist Security Info=True;TrustServerCertificate=True";
        protected string tabla;
        protected string Error = "";
        GenericValidator<T> validator;
        public GenericSQLServerController(GenericValidator<T> validator)
        {
            conexion = new SqlConnection(cadConexion);
            this.validator = validator;
            tabla = typeof(T).Name;
        }

        [HttpDelete("id")]
        public ActionResult<bool> Delete(string id)
        {
            try
            {
                conexion.Open();
                SqlCommand cmd = new SqlCommand($"Delete from {tabla} Where Id='{id}'", conexion);
                int r = cmd.ExecuteNonQuery();
                CerrarConexion();
                Error = "";
                return r == 1 ? Ok(true) : BadRequest(false);
            }
            catch (Exception ex)
            {
                CerrarConexion();
                return BadRequest(ex.Message);
            }
        }

        [HttpGet]
        public ActionResult<List<T>> Get()
        {
            var datos = ObtenerDatos($"select * from {tabla} for JSON AUTO");
            return datos != null ? Ok(datos) : BadRequest(Error);
        }

        [HttpGet("{id}")]
        public ActionResult<T> Get(string id)
        {
            var datos = ObtenerDatos($"select * from {tabla} where id='{id}' for JSON AUTO").SingleOrDefault();
            return datos != null ? Ok(datos) : BadRequest(Error);
        }

        [HttpPost]
        public ActionResult<T> Post([FromBody] T value)
        {
            try
            {
                value.Id = Guid.NewGuid().ToString();
                ValidationResult validationResult = validator.Validate(value);
                if (validationResult.IsValid)
                {
                    List<T> datos = new List<T>();
                    datos.Add(value);
                    DataTable table = ToDataTable<T>(datos);
                    conexion.Open();
                    int r = 0;
                    using (var cmd = new SqlCommand($"Select * from {tabla}", conexion))
                    {
                        using (var da = new SqlDataAdapter(cmd))
                        {
                            using (var cb = new SqlCommandBuilder(da))
                            {
                                r = da.Update(table);
                            }
                        }
                    }
                    CerrarConexion();
                    return r == 1 ? Get(value.Id) : BadRequest("Error al insertar las filas");
                }
                else
                {
                    string errors = "Error de validacion: ";
                    foreach (var item in validationResult.Errors)
                    {
                        errors += item.ErrorMessage + ". ";
                    }
                    return BadRequest(errors);
                }

            }
            catch (Exception ex)
            {
                CerrarConexion();
                return BadRequest(ex.Message);
            }
        }

        [HttpPut("{id}")]
        public ActionResult<T> Put(string id, [FromBody] T value)
        {
            try
            {
                ValidationResult validationResult = validator.Validate(value);
                if (validationResult.IsValid)
                {
                    List<T> datos = new List<T>();
                    datos.Add(value);
                    DataTable table = ToDataTable<T>(datos);
                    conexion.Open();
                    int r = 0;

                    var da = new SqlDataAdapter($"Select * from {tabla} where Id='{id}'", conexion);
                    DataTable dt = new DataTable();
                    da.Fill(dt);
                    dt.Rows[0].BeginEdit();
                    foreach (DataColumn colum in dt.Columns)
                    {
                        dt.Rows[0][colum.ColumnName] = table.Rows[0][colum.ColumnName];
                    }
                    dt.Rows[0].EndEdit();
                    //dt.Rows[0].SetModified();
                    var cb = new SqlCommandBuilder(da);
                    da.AcceptChangesDuringUpdate = true;
                    r = da.Update(dt);
                    CerrarConexion();
                    return r == 1 ? Get(value.Id) : BadRequest("Error al insertar las filas");
                }
                else
                {
                    string errors = "Error de validacion: ";
                    foreach (var item in validationResult.Errors)
                    {
                        errors += item.ErrorMessage + ". ";
                    }
                    return BadRequest(errors);
                }

            }
            catch (Exception ex)
            {
                CerrarConexion();
                return BadRequest(ex.Message);
            }
        }


        #region MetodosDeApoyo

        protected void CerrarConexion()
        {
            if (conexion.State == ConnectionState.Open)
            {
                conexion.Close();
            }
        }

        protected DataTable ToDataTable<T>(List<T> items)
        {
            DataTable dataTable = new DataTable(typeof(T).Name);

            //Get all the properties
            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo prop in Props)
            {
                //Setting column names as Property names
                dataTable.Columns.Add(prop.Name);
            }
            foreach (T item in items)
            {
                var values = new object[Props.Length];
                for (int i = 0; i < Props.Length; i++)
                {
                    //inserting property values to datatable rows
                    values[i] = Props[i].GetValue(item, null);
                }
                dataTable.Rows.Add(values);
            }
            //put a breakpoint here and check datatable
            return dataTable;
        }

        protected List<T> ObtenerDatos(string sql)
        {
            try
            {
                conexion.Open();
                StringBuilder json = new StringBuilder();
                using (var cmd = new SqlCommand(sql, conexion))
                {
                    var reader = cmd.ExecuteReader();
                    if (!reader.HasRows)
                    {
                        json.Append("[]");
                    }
                    else
                    {
                        while (reader.Read())
                        {
                            json.Append(reader.GetString(0).ToString());
                        }
                    }
                }
                CerrarConexion();
                Error = "";
                return JsonConvert.DeserializeObject<List<T>>(json.ToString());
            }
            catch (Exception ex)
            {
                CerrarConexion();
                Error = ex.Message;
                return null;
            }
        }
        #endregion
    }
}


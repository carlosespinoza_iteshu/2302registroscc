﻿using COMMON.Entidades;
using COMMON.Validadores;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LaboratorioController : GenericController<Laboratorio>
    {
        public LaboratorioController():base(new LaboratorioValidator())
        {

        }
    }
}

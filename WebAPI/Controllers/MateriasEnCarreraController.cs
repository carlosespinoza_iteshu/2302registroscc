﻿using COMMON.Entidades;
using COMMON.Validadores;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MateriasEnCarreraController : GenericController<MateriasEnCarrera>
    {
        public MateriasEnCarreraController():base(new MateriaEnCarreraValidator())
        {

        }
    }
}

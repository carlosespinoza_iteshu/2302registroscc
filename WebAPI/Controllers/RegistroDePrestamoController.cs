﻿using COMMON.Entidades;
using COMMON.Validadores;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RegistroDePrestamoController : GenericController<RegistroDePrestamo>
    {
        public RegistroDePrestamoController():base(new RegistroDePrestamoValidator())
        {

        }
    }
}

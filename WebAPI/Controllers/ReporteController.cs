﻿using COMMON.Entidades;
using COMMON.Validadores;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ReporteController : GenericController<Reporte>
    {
        public ReporteController():base(new ReporteValidator())
        {

        }

    }
}

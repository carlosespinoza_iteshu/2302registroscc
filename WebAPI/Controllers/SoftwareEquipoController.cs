﻿using COMMON.Entidades;
using COMMON.Validadores;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SoftwareEquipoController : GenericController<SoftwareEquipo>
    {
        public SoftwareEquipoController():base(new SoftwareEquipoValidator())
        {

        }
    }
}

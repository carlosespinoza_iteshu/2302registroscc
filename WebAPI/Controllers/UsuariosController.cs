﻿using COMMON.Entidades;
using COMMON.Modelos;
using COMMON.Validadores;
using LiteDB;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Data;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsuariosController : GenericController<Usuarios>
    {
        public UsuariosController() : base(new UsuariosValidator())
        {

        }

        [HttpGet("ObtenerUsuariosCompletos")]
        public ActionResult<List<ModelUsuarioListado>> ObtenerUsuariosCompletos()
        {
            List<Usuarios> usuarios = ObtenerDatos<Usuarios>();

            List<ModelUsuarioListado> listados = new List<ModelUsuarioListado>();

            List<Carrera> carreras = ObtenerDatos<Carrera>();

            var roles = ObtenerDatos<Rol>();
            foreach (var item in usuarios)
            {
                listados.Add(new ModelUsuarioListado()
                {
                    Apellidos = item.Apellidos,
                    Habilitado = item.Habilitado,
                    Id = item.Id,
                    IdCarrera = item.IdCarrera,
                    IdRol = item.IdRol,
                    Matricula = item.Matricula,
                    Nombre = item.Nombre,
                    Password = item.Password,
                    Carrera = carreras.SingleOrDefault(c => c.Id == item.IdCarrera).Nombre,
                    Imagen = ObtenNombreImagen(item.IdRol,roles),
                    Color = item.Habilitado ? "#00FF00" : "#FF0000" //RGB
                });
            }

            return Ok(listados);
        }

        

        private string ObtenNombreImagen(string idRol, List<Rol> roles)
        {
            Rol r = roles.SingleOrDefault(i => i.Id == idRol);
            if (r != null)
            {
                switch (r.Nombre)
                {
                    case "Docente":
                        return "docente.png";
                    case "Alumno":
                        return "alumno.png";
                    case "Laboratorista":
                        return "laboratorista.png";
                    default:
                        return "perfil.png";
                }
            }
            else
            {
                return "no.png";
            }
        }

        [HttpPost("Login")]
        public ActionResult<Usuarios> Login([FromBody] Login login)
        {
            try
            {
                Usuarios dato;
                using (var db = new LiteDatabase(DBName))
                {
                    var col = db.GetCollection<Usuarios>(typeof(Usuarios).Name);
                    dato = col.Find(u => u.Matricula == login.Matricula && u.Password == login.Password && u.Habilitado).SingleOrDefault();
                }
                if (dato != null)
                {
                    return Ok(dato);
                }
                else
                {
                    return BadRequest("Usuario y/o Contraseña incorrecta");
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }


    }
}
